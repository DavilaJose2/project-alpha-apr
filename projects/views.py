from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.


@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": list_projects}
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    show_project = get_object_or_404(Project, id=id)
    context = {"show_project": show_project}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            form.save()
            # To redirect to the detail view of the model, use this:
            return redirect("list_projects")

        # To add something to the model, like setting a user,
        # use something like this:
        #
        # model_instance = form.save(commit=False)
        # model_instance.user = request.user
        # model_instance.save()
        # return redirect("detail_url", id=model_instance.id)
    else:
        form = ProjectForm()

    context = {"form": form}

    return render(request, "projects/create.html", context)
