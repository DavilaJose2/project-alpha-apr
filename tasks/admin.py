from django.contrib import admin
from django.contrib.auth.models import User
from tasks.models import Task


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    ]
